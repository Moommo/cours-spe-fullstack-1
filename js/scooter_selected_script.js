const elementSelected = [1,2,4];

function chargeJSONData () {
  fetch('../data/scooter.json')
    .then(response => response.json())
    .then(data => {
      let scooterHTML = "";

      data.forEach(function (scooter) {
        if (elementSelected.includes(scooter.id)){

          if (scooter.state === 0) {
            scooterHTML += `
              <a class="scooter" href="../pages/scooter.html?id=${scooter.id}">
                <img src="${scooter.image}" alt="${scooter.name}">
                  <div class="scooter-info">
                    <h3>${scooter.name}</h3>
                    <p>${scooter.comment}</p>
                  </div>
                  <img src="../images/not_validate.png" alt="not_validate" style="width: 3vh">
              </a>`;
          } else {
            scooterHTML += `
              <div class="scooter">
                <img src="${scooter.image}" alt="${scooter.name}">
                  <div class="scooter-info">
                    <h3>${scooter.name}</h3>
                    <p>${scooter.comment}</p>
                  </div>
                  <img src="../images/validate.png" alt="validate" style="width: 3vh">
              </div>`;
          }
          document.getElementById("listScooter").innerHTML = scooterHTML;
        }
      });
    })
    .catch(error => console.error('Erreur de chargement du fichier JSON:', error));
}

function activeOrPassiveButton() {

  fetch('../data/scooter.json')
    .then(response => response.json())
    .then(jsonData => {

      const stateValues = jsonData.filter(item => elementSelected.includes(item.id))
        .map(item => item.state);      const btnSubmit = document.querySelector('input[type="submit"]');

      btnSubmit.disabled = !!stateValues.includes(0);

    })
    .catch(error => console.error('Error loading JSON:', error));
}

function onPageLoad() {
  chargeJSONData();
  activeOrPassiveButton()
}

window.addEventListener('load', onPageLoad);
