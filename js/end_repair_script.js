const elementSelected = [3,4,6];

function chargeJSONData () {
  fetch('../data/scooter.json')
    .then(response => response.json())
    .then(data => {
      let scooterHTML = "";

      data.forEach(function (scooter) {
        if (elementSelected.includes(scooter.id)){
          
          scooterHTML += `
          <div class="scooter">
            <img src="${scooter.image}" alt="${scooter.name}">
            <div class="scooter-info">
                <h3>${scooter.name}</h3>
                <ul class="ul-scooter-part"> 
                  <li>Rétroviseur (12EOOO)</li>
                  <li>Allumage (25OOP0)</li>
                 </ul>
            </div>
          </div>
          `;
          document.querySelector(".listScooter").innerHTML = scooterHTML;
        }
      });
    })
    .catch(error => console.error('Erreur de chargement du fichier JSON:', error));
}


function onPageLoad() {
  chargeJSONData();
}

window.addEventListener('load', onPageLoad);
