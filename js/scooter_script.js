const urlParams = new URLSearchParams(window.location.search);

const id = Number(urlParams.get('id'));

function chargeJSONDataScooter() {

  fetch('../data/scooter.json')
    .then(response => response.json())
    .then(data => {
      const targetScooter = data.find(scooter => scooter.id === id);

      if (targetScooter) {
        document.querySelector('.description-scooter').innerHTML += `
          <div class="scooter-detail">
            <div class="detail-input">
                <label for="model">Modèle : </label>
                <input id="model" type="text" readonly value="${targetScooter.model}">
            </div>
            <div class="detail-input">
                <label for="reference">Numéro de série : </label>
                <input id="reference" type="text" readonly value="${targetScooter.reference}">
            </div>
            <div class="detail-textarea">
                <label for="comment">Description du problème :</label>
                <textarea id="comment" readonly>Le scooter n'atteint pas sa vitesse maximale de plus, il semble que le pneu avant soit crevé
                </textarea>
            </div>
          </div>
        `;

      } else {
        console.error(`Scooter with id '${id}' not found.`);
      }
    });
}

function chargeJSONDataScooterParts() {

  fetch('../data/scooter_parts.json')
    .then(response => response.json())
    .then(data => {

      let scooterPartElement = "";

      data.forEach(function (scooterPart) {

        scooterPartElement += `
          <div class="scooter-part">
            <p> ${scooterPart.name} ( ${scooterPart.reference} ) </p>
            <input type="checkbox" name="scooter-part" value="${scooterPart.id}">
          </div>
        `;

      });

      scooterPartElement += `
        <div class="comment-object">
          <label for="comment"></label>
          <textarea id="comment" rows="5" cols="30" placeholder="Commentaire : "></textarea>
        </div>
      `;

      document.querySelector('.list-scooter-part').innerHTML = scooterPartElement;

    });
}

function onPageLoad() {
  chargeJSONDataScooter();
  chargeJSONDataScooterParts()
}

window.addEventListener('load', onPageLoad);
