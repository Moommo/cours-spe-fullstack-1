const checkboxes = document.getElementsByName('scooter');

function chargeJSONData () {
  fetch('../data/scooter.json')
    .then(response => response.json())
    .then(data => {

      data.forEach(function (scooter) {

        let scooterElement = "";

        data.forEach(function (scooter) {

          scooterElement += `
          <div class="scooter">
            <img src="${scooter.image}" alt="scooter${scooter.id}">
            <label for="scooter${scooter.id}">${scooter.name}</label>
            <input type="checkbox" id="scooter${scooter.id}" name="scooter" value="${scooter.id}" onchange="updateTitle()">
          </div>
        `;
        });
        document.getElementById("listScooter").innerHTML = scooterElement;
        updateTitle();
      });
    })
    .catch(error => console.error('Erreur de chargement du fichier JSON:', error));
}

function updateTitle() {
  let infoSelectedScooter = document.getElementById('info_selected_scooter');
  let checkedCount = Array.from(checkboxes).filter(checkbox => checkbox.checked).length;
  infoSelectedScooter.textContent = `Scooters sélectionnés pour l’intervention ( ${checkedCount} / ${checkboxes.length} ) `;
}


function onPageLoad() {
  chargeJSONData();
}

window.addEventListener('load', onPageLoad);
